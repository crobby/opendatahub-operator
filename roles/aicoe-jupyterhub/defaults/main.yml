---
state: "present"

registry: "{{ odh_spec['aicoe-jupyterhub']['registry'] | default('quay.io') }}"
repository: "{{ odh_spec['aicoe-jupyterhub']['repository'] | default('odh-jupyterhub') }}"
deploy_all_notebooks: "{{ odh_spec['aicoe-jupyterhub']['notebook_images']['deploy_all_notebooks'] | default(False) }}"
deploy_cuda_notebooks: "{{ odh_spec['aicoe-jupyterhub']['notebook_images']['deploy_cuda_notebooks'] | default(False) }}"
cookie_secret: "{{ odh_spec['aicoe-jupyterhub']['db_password'] | default('changeme') }}"

# Set the storage class when the value not empty string
# NOTE: Ansible jinja2 templating will convert any Null values to empty string.
#   This means we can never set the StorageClass to '' unless the ansible operator has env var DEFAULT_JINJA2_NATIVE=true
#   SEE: https://github.com/ansible/ansible/issues/57438
storage_class: "{{ odh_spec['aicoe-jupyterhub']['storage_class'] | default('') }}"
db_memory: "{{ odh_spec['aicoe-jupyterhub']['db_memory'] | default('1Gi') }}"
db_password: "{{ odh_spec['aicoe-jupyterhub']['db_password'] | default('changeme') }}"
jupyterhub_memory: "{{ odh_spec['aicoe-jupyterhub']['jupyterhub_memory'] | default('1Gi') }}"
notebook_image: "{{ odh_spec['aicoe-jupyterhub']['notebook_image'] | default('s2i-minimal-notebook:3.6') }}"
notebook_memory: "{{ odh_spec['aicoe-jupyterhub']['notebook_memory'] | default('2Gi') }}"
notebook_cpu: "{{ odh_spec['aicoe-jupyterhub']['notebook_cpu'] | default('1') }}"
s3_endpoint_url: "{{ odh_spec['aicoe-jupyterhub']['s3_endpoint_url'] | default('') }}"
gpu_mode: "{{ odh_spec['aicoe-jupyterhub']['gpu_mode'] | default('') }}"
jupyterhub_admins: "{{ odh_spec['aicoe-jupyterhub']['jupyterhub_admins'] | default([]) }}"
jupyterhub_db_version: "{{ odh_spec['aicoe-jupyterhub']['jupyterhub_db_version'] | default('9.6') }}"
user_pvc_size: "{{ odh_spec['aicoe-jupyterhub']['user_pvc_size'] | default('2Gi') }}"
jupyterhub_configmap_name: "{{ odh_spec['aicoe-jupyterhub']['jupyterhub_configmap_name'] | default('jupyterhub-cfg') }}"

spark_pyspark_submit_args: "{{ odh_spec['aicoe-jupyterhub']['spark_pyspark_submit_args'] | default('--conf spark.cores.max=6 --conf spark.executor.instances=2 --conf spark.executor.memory=3G --conf spark.executor.cores=3 --conf spark.driver.memory=4G --packages com.amazonaws:aws-java-sdk:1.7.4,org.apache.hadoop:hadoop-aws:2.7.3 pyspark-shell') }}"
spark_pyspark_driver_python: "{{ odh_spec['aicoe-jupyterhub']['spark_pyspark_driver_python'] | default('jupyter') }}"
spark_pyspark_driver_python_opts: "{{ odh_spec['aicoe-jupyterhub']['spark_pyspark_driver_python_opts'] | default('notebook') }}"
spark_home: "{{ odh_spec['aicoe-jupyterhub']['spark_home'] | default('/opt/app-root/lib/python3.6/site-packages/pyspark/') }}"
spark_pythonpath: "{{ odh_spec['aicoe-jupyterhub']['spark_pythonpath'] | default('$PYTHONPATH:/opt/app-root/lib/python3.6/site-packages/:/opt/app-root/lib/python3.6/site-packages/pyspark/python/:/opt/app-root/lib/python3.6/site-packages/pyspark/python/lib/py4j-0.8.2.1-src.zip') }}"

spark_configmap_template: "{{ odh_spec['aicoe-jupyterhub']['spark_configmap_template'] | default('jupyterhub-spark-operator-configmap') }}"

# Maintain backwards compatibility of the old flat list for SparkCluster variables
spark_memory: "{{ odh_spec['aicoe-jupyterhub']['spark_memory'] | default('2Gi') }}"
spark_cpu: "{{ odh_spec['aicoe-jupyterhub']['spark_cpu'] | default('1') }}"
spark_image: "{{ odh_spec['aicoe-jupyterhub']['spark']['image'] | default(odh_spec['aicoe-jupyterhub']['spark_image']) | default('quay.io/opendatahub/spark-cluster-image:spark22python36') }}"

spark_worker_nodes: "{{ odh_spec['aicoe-jupyterhub']['spark']['worker']['instances'] | default(odh_spec['aicoe-jupyterhub']['spark_worker_nodes']) | default('2') }}"
spark_master_nodes: "{{ odh_spec['aicoe-jupyterhub']['spark']['master']['instances'] | default(odh_spec['aicoe-jupyterhub']['spark_master_nodes']) | default('1') }}"

spark_master_memory_limit: "{{ odh_spec['aicoe-jupyterhub']['spark']['master']['resources']['limits']['memory'] | default(spark_memory) }}"
spark_master_cpu_limit: "{{ odh_spec['aicoe-jupyterhub']['spark']['master']['resources']['limits']['cpu'] | default(spark_cpu) }}"
spark_master_memory_request: "{{ odh_spec['aicoe-jupyterhub']['spark']['master']['resources']['requests']['memory'] | default(spark_memory) }}"
spark_master_cpu_request: "{{ odh_spec['aicoe-jupyterhub']['spark']['master']['resources']['requests']['cpu'] | default(spark_cpu) }}"
spark_worker_memory_limit: "{{ odh_spec['aicoe-jupyterhub']['spark']['worker']['resources']['limits']['memory'] | default(spark_memory) }}"
spark_worker_cpu_limit: "{{ odh_spec['aicoe-jupyterhub']['spark']['worker']['resources']['limits']['cpu'] | default(spark_cpu) }}"
spark_worker_memory_request: "{{ odh_spec['aicoe-jupyterhub']['spark']['worker']['resources']['requests']['memory'] | default(spark_memory) }}"
spark_worker_cpu_request: "{{ odh_spec['aicoe-jupyterhub']['spark']['worker']['resources']['requests']['cpu'] | default(spark_cpu) }}"
