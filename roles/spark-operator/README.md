Spark Operator
==============

This role deploys the [Radanalytics Spark Operator](https://github.com/radanalyticsio/spark-operator) using [ConfigMaps](https://github.com/radanalyticsio/spark-operator#config-map-approach) for `SparkCluster` and `SparkApplication` deployments.

Requirements
------------

None

Role Variables
--------------

All variables are expected to be located under `spark-operator` in the Open Data Hub custom resource `.spec`

* `odh_deploy` - Set to `true` if you want to deploy a spark operator with the ODH deployment
* `worker_node_count` - Number of worker nodes to include in the cluster that will be deployed immediatly after operator deployment
* `worker_memory` - Amount of memory to allocate to each worker node
* `worker_cpu` - Number of cpu(s) to allocate to each worker node
* `master_node_count` - Number of master nodes to include in the cluster that will be deployed immediatly after operator deployment
* `master_memory` - Amount of memory to allocate to each master node
* `master_cpu` - Number of cpu(s) to allocate to each master node
* `registry` - Registry where images are hosted
* `repository` - Repository where images are located in the registry
* `spark_image`: Custom spark image you want to use for the spark cluster. 
* `operator_image`: Custom spark image you want to use for the spark cluster.

Dependencies
------------

None

Sample Configuration
--------------------

```
  spark-operator:
    odh_deploy: true
    operator_image: radanalyticsio/spark-operator:latest
    spark_image: radanalyticsio/openshift-spark:latest
    
    worker:
      instances: 2
      resources:
        limits:
          memory: 4Gi
          cpu: 3
        requests:
          memory: 1Gi
          cpu: 500m
    master:
      instances: 1
      resources:
        limits:
          memory: 1Gi
          cpu: 1
        requests:
          memory: 512Mi
          cpu: 500m
```

License
-------

GNU GPLv3

Author Information
------------------

contributors@lists.opendatahub.io
